const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const saudacao = require('./saudacaoMid')

app.use(bodyParser.text())
app.use(saudacao('Higor'))

// app.use((req, res) => {
//     res.send('Restorno')
// })

app.get('/clientes/:id', (req, res) => {
    res.send(`Cliente ${req.params.id}`)
})

app.listen(3000, () => {
    console.log('Backend executando...')
})