import React from 'react'
import ReactDOM from 'react-dom'

// import BomDia from './componentes/BomDia'
// import Multi from './componentes/Multiplos'
//import {BoaTarde, BoaNoite } from './componentes/Multiplos'

import Pai from './componentes/Pai'
import Filho from './componentes/Filho'


ReactDOM.render(
    <div>
        <Pai nome="Paulo" sobrenome="Silva">
            {/* Como passo os componetnes filhos aqui */}
            <Filho nome="Pedro"/>
            <Filho nome="Paulo"/>
            <Filho nome="Carla"/>

        </Pai>
    </div>

, document.getElementById('root'))
